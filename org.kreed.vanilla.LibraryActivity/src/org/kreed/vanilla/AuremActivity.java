package org.kreed.vanilla;

/**
 * ���� ���� ������ ������ � ����� �� �������.
 * 
 * 1. ����� ���������� �������� ���������, ��� ������� �� ��� ��������� ���������
 * �������� � ���� �������� ��������� ������� ��������
 * 2. ����� ������������ (��� ����) ���������� ��������, ���/���� �����������, ��������
 * 3. ������������ � �������� �������, ������ �������, ������� ������������ ������
 * 
 */

import android.widget.SeekBar.OnSeekBarChangeListener;
import android.widget.SeekBar;

import java.io.File;

import android.content.DialogInterface;
import android.widget.EditText;
import android.app.AlertDialog;
import android.view.View;
import android.media.AudioManager;
import android.os.IBinder;
import android.content.ComponentName;
import android.content.Context;
import android.content.ServiceConnection;
import android.content.Intent;
import android.app.Activity;
import android.os.Bundle;

/**
 *  This is the Activity for the OpenPlayer Equalizer.
 */
public class AuremActivity extends Activity {


    private Intent intent;
    
    private EqualizerService eqService;

    private EqualizerModel model;

    private Intent listIntent;

    private EqualizerView eqView;

    public boolean isServiceOn;
    
    private SeekBar seekVolume;
    public static AudioManager audioStreem;
    
    private SeekBar seekBass;
    
	public static int totalNicks; 
	
	private VerticalSeekbar[] verticalSeekbar; 
	
	
    /**
     * ���������� ����� ���������� ��������
     * @param savedInstanceState Bundle ��������� ���������
     */
    @Override
    public void onCreate(Bundle savedInstanceState) {  	
    	
    	audioStreem = (AudioManager)getSystemService(Context.AUDIO_SERVICE); 
    	
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main_eq);


		
        intent = new Intent(this, EqualizerService.class);
        startService(intent);
        bindService(intent, serviceConnection, Context.BIND_AUTO_CREATE);
        isServiceOn = true; //true

        model = new EqualizerModel(this);

        //������� ���� ��������, ���� �� ��� ��������
        File file = new File("/sdcard/OpenPlayer/presets.txt");
        if(!file.exists()) {
            model.writePresetFile();
        }
        model.readPresetFile();

        file = new File("/sdcard/OpenPlayer/lastState.txt");
        if(!file.exists()) {
            for(short i = 0; i < 5; i ++) {
                model.setBandLevel(i, (short) 0);
            }
        }
        else {
            model.readLastStateFile();
        }

        verticalSeekbar = new VerticalSeekbar[5]; //*
        verticalSeekbar[0] = (VerticalSeekbar) findViewById(R.id.seekBar0);
        verticalSeekbar[1] = (VerticalSeekbar) findViewById(R.id.seekBar1);
        verticalSeekbar[2] = (VerticalSeekbar) findViewById(R.id.seekBar2);
        verticalSeekbar[3] = (VerticalSeekbar) findViewById(R.id.seekBar3);
        verticalSeekbar[4] = (VerticalSeekbar) findViewById(R.id.seekBar4);

        
        seekBass = (SeekBar) findViewById(R.id.BassBoostBar); //*
        seekVolume = (SeekBar) findViewById(R.id.VolumeLevelBar); //*
        
        seekBass.setMax(1500); //* max = 1000
        
        seekBass.setProgress(model.lBass);
       // seekBass.setProgress(0); // ���������� ��������� �������� ������ � 0
        seekBass.setOnSeekBarChangeListener(
                new SeekBassListener()); // ���������
                
        
        seekVolume.setMax(audioStreem.getStreamMaxVolume(AudioManager.STREAM_MUSIC));
        seekVolume.setProgress(audioStreem.getStreamVolume(AudioManager.STREAM_MUSIC)); // ������� ���������
        seekVolume.setOnSeekBarChangeListener(
                new SeekVolumeListener()); // ���������
        
        for(int i = 0; i < 5; i++) {
        	verticalSeekbar[i].setMax(3000);
        	verticalSeekbar[i].setProgress(model.getBandLevel((short) i) + 1500);
        	verticalSeekbar[i].setOnSeekBarChangeListener(
                new SeekBarListener());
        }

        eqView = (EqualizerView) findViewById(R.id.equalizerView);
        eqView.setModel(model);
        eqView.setActivity(this);
    }
    
	
    /**
     * Called when the application is resmued.
     */
    @Override
    public void onResume()
    {
        super.onResume();
        bindService(intent, serviceConnection, Context.BIND_AUTO_CREATE);
        isServiceOn = true;
    }

    /**
     * ���������� ����� ���������� ������ �����
     * @param view The view.
     */
    public void loadPresetClicked(View view) {
        String[] names = new String[10 + model.getPresets().size()];
        for(int i = 0; i < 10; i++) {
            names[i] = eqService.equalizer().getPresetName((short) i);
        }
        for (short i = 0; i < model.getPresets().size(); i++) {
            names[i + 10] = model.getPresets().get(i).getName();
        }

        this.onSaveInstanceState(new Bundle());

        listIntent = new Intent(this, PresetListView.class);
        listIntent.putExtra("names", names);
        this.startActivityForResult(listIntent, 666);
    }

    /**
     * ���������� ����� ���������� ������ savePreset 
     * @param view View the view being clicked.
     */
    public void savePresetClicked(View view)
    {
        AlertDialog.Builder alert = new AlertDialog.Builder(this);

        alert.setTitle("����� ������");
        alert.setMessage("��������� ��� �������.");

        final EditText input = new EditText(this);
        alert.setView(input);

        alert.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
        public void onClick(DialogInterface dialog, int whichButton) {
          String entered = input.getText().toString();
          short[] bands = new short[5];
          for(short i = 0; i < bands.length; i ++) {
              bands[i] = eqService.equalizer().getBandLevel(i);
          }
          model.createPreset(entered, bands);
          model.writePresetFile();
          }
        });

        alert.setNegativeButton("Cancel", new
            DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
            // Canceled.
          }
        });

        alert.show();

    }

    /**
     * ���������� ����� ������ On/Off ����������.
     * @param view View the view.
     */
    public void onOffClicked(View view)
    {
        if (isServiceOn == true) {
            eqService.equalizer().usePreset((short) 0);
            stopService(intent);
            isServiceOn = false;
        }
        else {

            intent = new Intent(this, EqualizerService.class);
            startService(intent);
            bindService(intent, serviceConnection, Context.BIND_AUTO_CREATE);
            isServiceOn = true;

            for(short i = 0; i < 5; i++) {
                eqService.equalizer().setBandLevel(i, model.getBandLevel(i));
            }
        }
    }

    /**
     * ���������� ����� ������ �������� �� ListActivity.
     * @param requestCode int the request code.
     * @param resultCode int the result code.
     * @param result Intent the resulting intent.
     *
     */
    @Override
    public void onActivityResult(int requestCode, int resultCode,
        Intent result)
    {
        if(requestCode == 666 && resultCode == RESULT_OK) {
            short resultingPreset = (short) result.getIntExtra("index", 0);
            if(resultingPreset <= 9) {
                eqService.equalizer().usePreset( (short)
                    result.getIntExtra("index", 0));
                short[] bandLevels = new short[5];
                for(short i = 0; i < 5; i ++) {
                   bandLevels[i] = eqService.equalizer().getBandLevel(i);
                   model.setBandLevel(i, bandLevels[i]);
                   verticalSeekbar[i].setProgress(bandLevels[i] + 1500);
                }
            }
            else {
                Preset preset = model.getPreset((short)(resultingPreset - 10));
                short[] bands = preset.getBands();
                for(short i = 0; i < bands.length; i++) {
                    eqService.equalizer().setBandLevel(i, bands[i]);
                }
                short[] bandLevels = new short[5];
                for(short i = 0; i < 5; i ++) {
                   bandLevels[i] = eqService.equalizer().getBandLevel(i);
                   model.setBandLevel(i, bandLevels[i]);
                   verticalSeekbar[i].setProgress(bandLevels[i] + 1500);
                }
            }
        }
    }

    /**
     * Called when a new intent starts the activity.
     * @param newIntent Intent the new intent.
     */
    @Override
    public void onNewIntent(Intent newIntent)
    {
    	
    }

    /**
     * ���������� ����� ���������� ������������
     */
    @Override
    public void onDestroy()
    {
        super.onDestroy();
        model.writeLastStateFile(eqService.BassBoost().getRoundedStrength());
        unbindService(serviceConnection);
    }
    
    /**
     * ��������� ���������� ���� ������������� ������� �� ��
     * ��������� ��� ������ � ������� ���������� �����, ��� ����� ����� 
     * �������� ������, ������������ ������ ���������� ��� �����������
     */
    /*
    @Override
    public void onPause()
    {
        super.onPause();
        
        eqService.equalizer().usePreset((short) 0);
        stopService(intent);
        isServiceOn = false;
    }
	*/
    /**
     * ��������� ��������� �����������
     * This is called by the framework to save the state of
     * the activity. Which in our case is the band levels present
     * in the equalizer.
     */
    @Override
    public void onSaveInstanceState(Bundle bundle)
    {
        super.onSaveInstanceState(bundle);
        short[] bands = new short[5];
        for(short i = 0; i < 5; i++) {
            bands[i] = eqService.equalizer().getBandLevel(i);
        }

        bundle.putShortArray("bandLevels", bands);


    }


    /**
     *  ��������� ��������� � ����������
     *  This is a listener that responds to changes in the five
     *  seek bars of the equalizer.
     *
     */
    public class SeekBarListener implements OnSeekBarChangeListener
    {

        /**
         * ���������� ����� ����� ������ seekBars ����������
         * Called when any of the seekBars are changed.
         * @param seekBar SeekBar the bar.
         * @param progress int the level of the bar.
         * @param fromUser boolean true if the change comes from the user.
         */
        public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
        	
            int theProgress = progress - 1500;
	        
            for(int i = 0; i < 5; i++) {
                if(seekBar.equals(verticalSeekbar[i])) {
                    model.setBandLevel((short) i, (short) theProgress);
                    eqService.equalizer().setBandLevel((short) i,
                        (short) theProgress);	
                }
            }
        }
   

        /**
         * called when touch tracking starts.
         * @param seekBar the seek bar.
         */
        public void onStartTrackingTouch(SeekBar seekBar)
        {

        }

        /**
         * called when touch tracking stops.
         * @param seekBar the seek bar.
         */
        public void onStopTrackingTouch(SeekBar seekBar)
        {

        }

    }
    
    public class SeekBassListener implements OnSeekBarChangeListener
    {

        /**
         * ���������� ����� SeekBass ����������
         * @param seekBar SeekBar the bar.
         * @param progress int the level of the bar.
         * @param fromUser boolean true if the change comes from the user.
         */
    
		public void onProgressChanged(SeekBar seekBar, int progress,
				boolean fromUser) {
			eqService.BassBoost().setStrength((short) (progress));
		}

		public void onStartTrackingTouch(SeekBar seekBar) {

		}

		public void onStopTrackingTouch(SeekBar seekBar) {

		}
    }
    
    /**
     * seekBar ��� ��������� ���������
     */
    public class SeekVolumeListener implements OnSeekBarChangeListener
    {

		public void onProgressChanged(SeekBar seekBar, int progress,
				boolean fromUser) {
			audioStreem.setStreamVolume(AudioManager.STREAM_MUSIC,
					progress, AudioManager.FLAG_PLAY_SOUND);
		}

		public void onStartTrackingTouch(SeekBar seekBar) {
			
		}

		public void onStopTrackingTouch(SeekBar seekBar) {
			
		}
    	
    }

    public ServiceConnection serviceConnection = new ServiceConnection() {
        public void onServiceConnected(ComponentName className,
            IBinder service) {
            eqService =
                ((EqualizerService.ServiceBinder) service).getService();
            for(short i = 0; i < 5; i ++) {
                eqService.equalizer().setBandLevel(i, model.getBandLevel(i));
            }
        }

        public void onServiceDisconnected(ComponentName className) {
            eqService = null;
        }
    };
}