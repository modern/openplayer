package org.kreed.vanilla;

import android.graphics.Canvas;
import android.content.Context;
import android.util.AttributeSet;

public class LabelView extends android.view.View
{
    /**
     * Constructor for LabelView objects.
     * @param context Context the context.
     * @param attrs AttributeSet the attributes.
     */
    public LabelView(Context context, AttributeSet attrs)
    {
        super(context, attrs);
        //Construct.
    }

    /**
     * Called when the view is drawn.
     * @param canvas Canvas the canvas.
     */
    @Override
    public void onDraw(Canvas canvas)
    {
	
    }
}
